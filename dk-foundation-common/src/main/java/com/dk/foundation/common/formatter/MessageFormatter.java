package com.dk.foundation.common.formatter;

import ch.qos.logback.classic.spi.ILoggingEvent;

/**
 * Created by duguk on 2018/10/17.
 */
public class MessageFormatter implements Formatter {

    public String format(ILoggingEvent event) {
        return event.getFormattedMessage();
    }

}
