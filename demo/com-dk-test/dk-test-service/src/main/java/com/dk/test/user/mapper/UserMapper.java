package com.dk.test.user.mapper;

import com.dk.test.entity.user.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;


/**
 * Created by duguk on 2018/1/9.
 */

@Mapper
public interface UserMapper {
//    @Select("select test_id as id, name, age, test_type from user")
    List<User> selectListBySQL();
}
