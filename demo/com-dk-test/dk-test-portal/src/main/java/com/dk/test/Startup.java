package com.dk.test;

import com.dk.foundation.annotation.EnableEngineStart;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * Created by duguk on 2018/1/9.
 */
@MapperScan("com.dk.test.*.mapper")
@EnableEngineStart
//@EnableDiscoveryClient
public class Startup {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Startup.class, args);
    }
}
